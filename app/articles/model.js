import Model from '../model'

const Articles = {
    ...Model,
    key: 'articles',
    permittesAttrs: ['title', 'authorId']
}
export default Articles