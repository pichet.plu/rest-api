import Serializer from '../serializer'

const SessionsSerailizer = {
    ...Serializer,

    create(user) {
        const { id, email, isAdmin } = user

        return { id, email, isAdmin }
    }
}
export default SessionsSerailizer